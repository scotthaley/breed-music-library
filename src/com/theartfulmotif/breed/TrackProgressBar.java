package com.theartfulmotif.breed;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JComponent;

public class TrackProgressBar extends JComponent implements MouseListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9069925982186216427L;
	
	private int MaxValue = 100;
	private int CurrentValue = 20;
	private long marker1 = 0;

	public TrackProgressBar()
	{
		this.setOpaque(false);
		this.addMouseListener(this);
	}
	
	public void setMaximum(int max)
	{
		MaxValue = max;
		repaint();
	}
	
	public void setValue(int value)
	{
		CurrentValue = value;
		repaint();
	}
	
	public void setMarkers(long mark1)
	{
		marker1 = mark1;
	}
	
	@Override
	public void paintComponent(Graphics g)
	{
		Dimension dim = getSize();
		drawMarkers(g, dim);
		
		g.setColor(Color.WHITE);
		g.fillRect(0, dim.height / 2 - 2, dim.width, dim.height / 3);
		
		setProgress(g, dim);
		
		g.setColor(Color.GRAY);
		g.drawRect(0,dim.height / 2 - 3, dim.width - 1, dim.height / 3 + 1);
	}
	
	public void setProgress(Graphics g, Dimension dim)
	{
		int pixelPos = (int)(((float)CurrentValue / (float)MaxValue) * dim.width);
		g.setColor(Color.DARK_GRAY);
		g.fillRect(0, dim.height / 2 - 2, pixelPos, dim.height / 3);
	}
	
	public void drawMarkers(Graphics g, Dimension dim)
	{
		if (marker1 != 0)
		{
			g.setColor(Color.BLUE);
			int pixelPos = (int)(((float)marker1 / (float)MaxValue) * dim.width);
			g.fillRect(pixelPos, 0, 3, dim.height);
		}
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		Dimension dim = getSize();
		int newProgress = (int)(((float)arg0.getX() / (float)dim.width) * MaxValue);
		AudioPlayer.getInstance().Seek(newProgress);
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
}
