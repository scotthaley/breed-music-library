package com.theartfulmotif.breed;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;


public class InfoPanel extends javax.swing.JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9093266564720699112L;

	private static InfoPanel instance = null;
	
	private int ProgressBarWidth;
	private JProgressBar Info_Progress;
	private JLabel Info_SongTitle;
	private JLabel Info_Composer;
	private JLabel Info_CurrentPosition;
	private JLabel Info_TimeLeft;
	private TrackProgressBar Info_ProgressBar;
	
	public long TotalTime;
	
	public static InfoPanel getInstance()
	{
		if (instance == null)
			instance = new InfoPanel();
		return instance;
	}
	
	private InfoPanel()
	{
		this.setOpaque(false);
		this.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		this.setPreferredSize(new Dimension(800, 80));
		
		this.setLayout(new GridLayout(3, 0, 0, 0));
		
		Info_SongTitle = new JLabel("Song Title");
		Info_SongTitle.setFont(new Font("SansSerif", Font.BOLD, 14));
		Info_SongTitle.setHorizontalAlignment(SwingConstants.CENTER);
		this.add(Info_SongTitle);
		
		Info_Composer = new JLabel("Composer");
		Info_Composer.setFont(new Font("SansSerif", Font.PLAIN, 11));
		Info_Composer.setHorizontalAlignment(SwingConstants.CENTER);
		this.add(Info_Composer);
		
		JPanel AudioProgressPanel = new JPanel();
		AudioProgressPanel.setOpaque(false);
		this.add(AudioProgressPanel);
		FlowLayout fl_AudioProgressPanel = new FlowLayout();
		AudioProgressPanel.setLayout(fl_AudioProgressPanel);
		
		Info_CurrentPosition = new JLabel("12:34");
		Info_CurrentPosition.setFont(new Font("SansSerif", Font.PLAIN, 11));
		Info_CurrentPosition.setHorizontalAlignment(SwingConstants.LEFT);
		AudioProgressPanel.add(Info_CurrentPosition);
		
		Info_Progress = new JProgressBar();
		Info_Progress.setMaximum(1000);
		Info_Progress.setValue(500);
		ProgressBarWidth = (int)(700);
		Info_Progress.setPreferredSize(new Dimension(ProgressBarWidth, 17));
		Info_Progress.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e) {
				ProgressBarClicked(e);
			}
		});
		//AudioProgressPanel.add(Info_Progress);
		
		Info_ProgressBar = new TrackProgressBar();
		Info_ProgressBar.setPreferredSize(new Dimension(ProgressBarWidth,17));
		AudioProgressPanel.add(Info_ProgressBar);
		
		Info_TimeLeft = new JLabel("-56:78");
		Info_TimeLeft.setFont(new Font("SansSerif", Font.PLAIN, 11));
		Info_TimeLeft.setHorizontalAlignment(SwingConstants.RIGHT);
		AudioProgressPanel.add(Info_TimeLeft);
	}
	
	public void SetMarkers(long mark1)
	{
		Info_ProgressBar.setMarkers(mark1);
	}
	
	public void SetInfo(String title, String composer)
	{
		Info_SongTitle.setText(title);
		Info_Composer.setText(composer);
	}
	
	public void SetTotalTime(long time)
	{
		if (time == TotalTime)
			return;
		TotalTime = time;
		
		int seconds = (int)(time / 1000000);
		int minutes = (int)(seconds / 60);
		int secondsLeft = (int)(seconds % 60);
		Info_TimeLeft.setText("-" + Integer.toString(minutes) + ":" + String.format("%02d",secondsLeft));
		
		Info_ProgressBar.setMaximum((int)TotalTime);
	}
	
	public void SetCurrentTime(long time)
	{
		int seconds = (int)(time / 1000000);
		int minutes = (int)(seconds / 60);
		int secondsLeft = (int)(seconds % 60);
		Info_CurrentPosition.setText(Integer.toString(minutes) + ":" + String.format("%02d",secondsLeft));
		
		seconds = (int)((TotalTime - time) / 1000000);
		minutes = (int)(seconds / 60);
		secondsLeft = (int)(seconds % 60);
		Info_TimeLeft.setText("-" + Integer.toString(minutes) + ":" + String.format("%02d",secondsLeft));
		
		//int newProgress = (int)(time / TotalTime * 1000);
		Info_ProgressBar.setValue((int)time);
	}
	
	private void ProgressBarClicked(MouseEvent e)
	{
		int newProgress = (int)((float)e.getX() / ProgressBarWidth * 1000);
		Info_Progress.setValue(newProgress);
	}
	
	@Override
    protected void paintComponent(Graphics grphcs) {
        Graphics2D g2d = (Graphics2D) grphcs;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        GradientPaint gp = new GradientPaint(0, 0,
                new Color(231,235,209), 0, getHeight(),
                new Color(212,217,184));

        g2d.setPaint(gp);
        g2d.fillRect(0, 0, getWidth(), getHeight());

        super.paintComponent(grphcs);
    }
}
