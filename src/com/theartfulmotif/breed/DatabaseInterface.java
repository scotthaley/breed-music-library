package com.theartfulmotif.breed;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DatabaseInterface {
	
	private static DatabaseInterface instance = null;
	
	Connection mConnection = null;
	
	public static DatabaseInterface getInstance()
	{
		if (instance == null)
			instance = new DatabaseInterface();
		return instance;
	}
	
	private DatabaseInterface()
	{
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println("Where is your MySQL JDBC Driver?");
			e.printStackTrace();
			return;
		}
		
		try
		{
			// Local = 192.168.1.74
			mConnection = DriverManager.getConnection("jdbc:mysql://99.136.94.212:3306/BreedMusic","ScottHaley","testpass");
		} catch (SQLException e)
		{
			System.out.println("Connection Failed!");
			e.printStackTrace();
			return;
		}
		
		if (mConnection != null)
			System.out.println("MySQL Connected!");
		else
			System.out.println("There was a problem connecting to MySQL! :(");
		
	}
	
	public void query(String query)
	{
		System.out.println("query-" + query);
		try {
			Statement stmt = mConnection.createStatement();
			stmt.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public List<String> queryList(String query, int column)
	{
		System.out.println("query-" + query);
		List<String> returnList = new ArrayList<String>();
		
		try {
			Statement stmt = mConnection.createStatement();
			
			ResultSet rs = stmt.executeQuery( query);
			
			while (rs.next())
				returnList.add( rs.getString(column));
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return returnList;
	}
	
	public Map<String,String> queryMap(String query)
	{
		System.out.println("query-" + query);
		Map<String,String> returnMap = new HashMap<String,String>();
		try {
			Statement stmt = mConnection.createStatement();
			
			ResultSet rs = stmt.executeQuery( query);
			ResultSetMetaData rsMetaData = rs.getMetaData();
			for (int c = 0; c < rsMetaData.getColumnCount(); c++)
			{
				returnMap.put(rsMetaData.getColumnName(c), rs.getString(c));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return returnMap;
	}
	
	public List<Map<String,String>> queryMapList(String query)
	{
		System.out.println("query-" + query);
		List<Map<String,String>> returnList = new ArrayList<Map<String,String>>();
		try {
			Statement stmt = mConnection.createStatement();
			
			ResultSet rs = stmt.executeQuery( query);
			ResultSetMetaData rsMetaData = rs.getMetaData();
			
			while(rs.next())
			{
				Map<String,String> returnMap = new HashMap<String,String>();
				for (int c = 1; c <= rsMetaData.getColumnCount(); c++)
				{
					returnMap.put(rsMetaData.getColumnName(c), rs.getString(c));
				}
				returnList.add(returnMap);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return returnList;
	}
}
