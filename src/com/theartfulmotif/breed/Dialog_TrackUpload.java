package com.theartfulmotif.breed;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;

import com.theartfulmotif.breed.ListControllers.TrackController;

public class Dialog_TrackUpload extends JDialog implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2490151574679625747L;
	private final JPanel contentPanel = new JPanel();
	private JTextField fileTextField;

	final JFileChooser fc = new JFileChooser();
	private JTextField Title;
	private JTextField Composer;
	
	private TrackController Controller_Track;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			Dialog_TrackUpload dialog = new Dialog_TrackUpload();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public Dialog_TrackUpload() {
		setTitle("Upload Track");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			fileTextField = new JTextField();
			fileTextField.setEditable(false);
			contentPanel.add(fileTextField);
			fileTextField.setColumns(27);
		}
		{
			JButton btnFileSelect = new JButton("Select File");
			contentPanel.add(btnFileSelect);
			btnFileSelect.setActionCommand("select_file");
			{
				JLabel lblTitle = new JLabel("Title");
				contentPanel.add(lblTitle);
			}
			{
				Title = new JTextField();
				contentPanel.add(Title);
				Title.setColumns(30);
			}
			{
				JLabel lblComposer = new JLabel("Composer");
				contentPanel.add(lblComposer);
			}
			{
				Composer = new JTextField();
				contentPanel.add(Composer);
				Composer.setColumns(30);
			}
			btnFileSelect.addActionListener(this);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Upload");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
				okButton.addActionListener(this);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
	
	public void setTrackController(TrackController controller)
	{
		Controller_Track = controller;
	}
	
	public void actionPerformed(ActionEvent e)
	{
		if (e.getActionCommand() == "select_file")
		{
			int returnVal = fc.showOpenDialog(this);
			if (returnVal == JFileChooser.APPROVE_OPTION)
			{
				String file = fc.getSelectedFile().getAbsolutePath();
				if (file.toLowerCase().endsWith(".aif"))
					fileTextField.setText(file);
				else
					JOptionPane.showMessageDialog(this, "File is not in the .aif format.");
			} 
		}
		if (e.getActionCommand() == "OK")
		{
			String fName = SFTPManager.getInstance().putFile(fileTextField.getText());
			System.out.println("File uploaded.");
			String url = SFTPManager.rootDirectory + fName.substring(0,fName.lastIndexOf(".")) + ".mp3";
			String cmd = "ffmpeg -i " + SFTPManager.rootDirectory + fName + " -f mp3 -ab 192 -ar 44100 " + url;
			System.out.println(cmd);
			SFTPManager.getInstance().exec(cmd);
			BufferedReader br = SFTPManager.getInstance().getReader();
			String line, size = "test";
			try {
				while ((line = br.readLine()) != null)
				{
					if (line.contains("audio"))
					{
						if (line.toLowerCase().contains("kb"))
						{
							int audioIndex = line.indexOf("audio") + 6;
							size = line.substring(audioIndex, line.toLowerCase().indexOf("kb", audioIndex));
							size = String.valueOf(Integer.parseInt(size) * 1000);
							break;
						} else if (line.toLowerCase().contains("mb"))
						{
							int audioIndex = line.indexOf("audio") + 6;
							size = line.substring(audioIndex, line.toLowerCase().indexOf("mb", audioIndex));
							size = String.valueOf(Integer.parseInt(size) * 10000);
							break;
						}
					}
				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			String title = Title.getText();
			String composer = Composer.getText();
			DatabaseInterface.getInstance().query("INSERT INTO Track (title,composer,url,size) VALUES(\"" + title + "\", \"" + composer + "\",\"sftp:/" + url + "\"," + size +");");
			Controller_Track.Refresh();
			setVisible(false);
		}
	}

}
