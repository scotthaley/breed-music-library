package com.theartfulmotif.breed;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class ListItemTitle extends ListItem {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3790314394382200678L;

	JLabel Title;
	
	public ListItemTitle(String title) {
		super();


		Title = new JLabel(title);
		Title.setForeground(Color.BLACK);
		Title.setHorizontalAlignment(SwingConstants.LEFT);
		Title.setFont(new Font("SansSerif", Font.BOLD, 11));
		add(Title);
	}
	
	@Override
	public void Unselect(Boolean origin)
	{
		super.Unselect(origin);
		Title.setForeground(Color.BLACK);
	}
	
	@Override
	public void Select(Boolean origin)
	{
		super.Select(origin);
		Title.setForeground(new Color(255,255,255));
		Select(Title.getText());
	}

}
