package com.theartfulmotif.breed;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.Box;
import javax.swing.JPanel;

public class VideoPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8779773874200456581L;
	
	private ButtonPanel OpenButton;
	private VideoPlayer mVideoPlayer;
	
	public VideoPanel()
	{
		this.setBackground(Color.DARK_GRAY);
		this.setPreferredSize(new Dimension(100, 25));
		this.setLayout(new GridBagLayout());
		
		OpenButton = new ButtonPanel("Video")
		{
			private static final long serialVersionUID = 3289197060991658804L;

			@Override
			public void OnClick()
			{
				OpenPanel();
			}
		};
		OpenButton.setLabelForeground(Color.WHITE);
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 1;
		this.add(Box.createHorizontalGlue(), c);
		
		c.gridx = 2;
		this.add(Box.createHorizontalGlue(), c);
		
		
		c.gridx = 1;
		c.weightx = 0;
		this.add(OpenButton, c);
		
		c.gridy = 1;
		
		mVideoPlayer = new VideoPlayer();
		mVideoPlayer.setVisible(false);
		this.add(mVideoPlayer, c);
	}

	public void OpenPanel()
	{
		OpenButton.setVisible(false);
		mVideoPlayer.setVisible(true);
		mVideoPlayer.PlayTest();
		this.setPreferredSize(new Dimension(100, 250));
	}
}
