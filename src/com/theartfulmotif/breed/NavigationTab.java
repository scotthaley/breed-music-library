package com.theartfulmotif.breed;

import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.FlowLayout;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class NavigationTab extends JPanel implements MouseListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -590427323572611292L;
	
	JLabel Title;
	
	public boolean Over = false;
	public boolean Selected = false;

	/**
	 * Create the panel.
	 */
	public NavigationTab(String title, int width) {
		FlowLayout flowLayout = (FlowLayout) getLayout();
		flowLayout.setVgap(2);
		flowLayout.setHgap(15);
		flowLayout.setAlignment(FlowLayout.LEFT);
		this.setPreferredSize(new Dimension(width, 20));
		
		this.setOpaque(false);
		
		Title = new JLabel(title);
		Title.setForeground(new Color(68, 97, 92));
		Title.setHorizontalAlignment(SwingConstants.LEFT);
		Title.setFont(new Font("SansSerif", Font.BOLD, 11));
		add(Title);
		
		this.addMouseListener(this);
	}
	
	public void MouseOver()
	{
		
	}
	
	public void Select(String title)
	{
		
	}
	
	public void Unselect()
	{
		Selected = false;
		this.repaint();
		Title.setForeground(new Color(68, 97, 92));
	}
	
	public void ClearOver()
	{
		Over = false;
		this.repaint();
	}

	@Override
    protected void paintComponent(Graphics grphcs) {
		if (Selected)
		{
			Graphics2D g2d = (Graphics2D) grphcs;
	        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
	                RenderingHints.VALUE_ANTIALIAS_ON);
	
	        GradientPaint gp = new GradientPaint(0, 0,
	                new Color(89, 91, 222), 0, getHeight(),
	                new Color(45,48,207));
	
	        g2d.setPaint(gp);
	        g2d.fillRect(0, 0, getWidth(), getHeight());
	
	        super.paintComponent(grphcs);
		}
		else if (Over)
		{
			Graphics2D g2d = (Graphics2D) grphcs;
	        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
	                RenderingHints.VALUE_ANTIALIAS_ON);
	
	        GradientPaint gp = new GradientPaint(0, 0,
	                getBackground().brighter(), 0, getHeight(),
	                getBackground().darker());
	
	        g2d.setPaint(gp);
	        g2d.fillRect(0, 0, getWidth(), getHeight());
	
	        super.paintComponent(grphcs);
		}
	}

	@Override
	public void mouseEntered(MouseEvent e)
	{
		MouseOver();
		Over = true;
		this.repaint();
	}
	
	@Override
	public void mouseClicked(MouseEvent arg0) {
		Selected = true;
		this.repaint();
		Title.setForeground(new Color(255,255,255));
		Select(Title.getText());
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		ClearOver();
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
