package com.theartfulmotif.breed;

import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class ListItemImage extends ListItem {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5830089053673617600L;

	JLabel imageLabel;
	
	public ListItemImage(String imageURL)
	{
		super(0);
		ImageIcon image = new ImageIcon(getClass().getResource(imageURL));
		imageLabel = new JLabel("", image, JLabel.CENTER);
		imageLabel.addMouseListener(this);
		this.add(imageLabel);
	}

	public void Play()
	{
		
	}
	
	@Override
	public void mouseClicked(MouseEvent arg0) {
		super.mouseClicked(arg0);
		
		if (arg0.getSource() == imageLabel)
		{
			Play();
		}
	}

	public void setImageVisible(Boolean visible)
	{
		imageLabel.setVisible(visible);
	}
}
