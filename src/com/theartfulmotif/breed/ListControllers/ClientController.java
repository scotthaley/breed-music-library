package com.theartfulmotif.breed.ListControllers;

import java.util.List;

import com.theartfulmotif.breed.DatabaseInterface;
import com.theartfulmotif.breed.ListItemController;

public class ClientController extends ListItemController {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6487456585442004962L;

	public ClientController() {
		super(150);
		
		List<String> clients = DatabaseInterface.getInstance().queryList("SELECT * FROM Client", 2);
		for (String c : clients)
		{
			AddItem(c);
		}
	}

}
