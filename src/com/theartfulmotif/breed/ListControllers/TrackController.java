package com.theartfulmotif.breed.ListControllers;

import java.util.List;
import java.util.Map;

import com.theartfulmotif.breed.AudioPlayer;
import com.theartfulmotif.breed.DatabaseInterface;
import com.theartfulmotif.breed.ListItemController;
import com.theartfulmotif.breed.TrackTopListItem;


public class TrackController extends ListItemController {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7421904151550241198L;
	
	private TrackTopListItem mTopItem;
	List<Map<String,String>> TrackData;
	int CurrentSong = 0;

	public TrackController() {
		super(400);
		
		LoadTracks();
		
		/*
		ActionListener taskPerformer = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
            	SwitchSong();
            }
		};
		
		//Timer mTimer = new Timer(1000, taskPerformer);
		//mTimer.start();
		 */
	}
	
	public void Refresh()
	{
		RemoveAllItems();
		LoadTracks();
		this.repaint();
		this.invalidate();
		this.getParent().validate();
	}
	
	public void LoadTracks()
	{
		mTopItem = new TrackTopListItem();
		AddItem(mTopItem,true);
		
		TrackData = DatabaseInterface.getInstance().queryMapList("SELECT * FROM Track ORDER BY title");
		
		for (Map<String,String> tInfo : TrackData)
		{
			long mark1 = 0;
			if (tInfo.get("mark_1") != null)
			{
				mark1 = Long.parseLong(tInfo.get("mark_1"));
			}
			AddItem(tInfo.get("title"), tInfo.get("composer"), tInfo.get("url"), mark1, Integer.parseInt(tInfo.get("size")));
		}
	}
	
	public void SwitchSong()
	{
		System.out.println("Playing: " + TrackData.get(CurrentSong).get("title"));
		AudioPlayer.getInstance().SetLocation(TrackData.get(CurrentSong).get("url"), Integer.parseInt(TrackData.get(CurrentSong).get("size"))).Play(0);
		CurrentSong ++;
		if (CurrentSong > 4)
			CurrentSong = 0;
	}


}
