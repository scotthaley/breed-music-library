package com.theartfulmotif.breed;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelShell;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class SFTPManager {

	private static SFTPManager instance;
	
	private ChannelSftp sftpChannel;
	private ChannelShell execChannel;
	private PrintStream printStream;
	private InputStream mInputStream;
	public static String rootDirectory = "/Users/lawrencerogers/Music/Breed/";
	
	public static SFTPManager getInstance()
	{
		if (instance == null)
			instance = new SFTPManager();
		return instance;
	}
	
	private SFTPManager()
	{
		Connect();
	}
	
	private void Connect()
	{
		JSch jsch = new JSch();
		Session session = null;
		
		try {
			//72.54.90.206
			//192.168.0.5
			/*
			session = jsch.getSession("java_test", "72.54.90.206", 22);
			session.setConfig("StrictHostKeyChecking", "no");
	        session.setPassword("java_test");
	        */
			session = jsch.getSession("lawrencerogers", "192.168.1.74", 22);
			session.setConfig("StrictHostKeyChecking", "no");
	        session.setPassword("riofrio007");
	        session.connect();

	        Channel channel = session.openChannel("sftp");
	        channel.connect();
	        sftpChannel = (ChannelSftp) channel;
	        execChannel = (ChannelShell) session.openChannel("shell");
	        // This line prints the shell output to the console
	        //execChannel.setOutputStream(System.out);
	        try {
				printStream = new PrintStream(execChannel.getOutputStream(), true);
			} catch (IOException e) {
				e.printStackTrace();
			}
	        System.out.println("Sftp connection opened.");
	        execChannel.connect();
		} catch (JSchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public InputStream getFileStream(String url) throws IOException
	{
		if (!sftpChannel.isConnected())
		{
			System.out.println("Connection closed.");
			Connect();
		}
		try {
			if (mInputStream != null)
				mInputStream.close();
			mInputStream = null;
			mInputStream = sftpChannel.get(url);
			return mInputStream;
		} catch (SftpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		return null;
	}
	
	public String putFile(String fileUrl)
	{
		File f = new File(fileUrl);
		try {
			sftpChannel.cd(rootDirectory);
			sftpChannel.put(new FileInputStream(f), f.getName());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (SftpException e) {
			e.printStackTrace();
		}
		return f.getName();
	}
	
	public void exec(String command)
	{
		if (!execChannel.isConnected())
			Connect();
		printStream.println(command);
	}
	
	public BufferedReader getReader()
	{
		try {
			return new BufferedReader( new InputStreamReader(execChannel.getInputStream()));
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}
