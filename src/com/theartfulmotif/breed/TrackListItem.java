package com.theartfulmotif.breed;

import java.awt.Color;
import java.awt.GridBagConstraints;

import javax.swing.JPanel;
import javax.swing.border.MatteBorder;

public class TrackListItem extends ListItemTitle {

	/**
	 * 
	 */
	private static final long serialVersionUID = -236208633250799709L;
	
	ListItemTitle mComposerItem;
	ListItemImage mPlayItem;
	
	String mURL;
	String mTitle;
	String mComposer;
	long mMarker1;
	int mSize = 0;

	public TrackListItem(String title, String composer, String url, long marker1, int size) {
		super(title);
		
		mURL = url;
		mTitle = title;
		mComposer = composer;
		mMarker1 = marker1;
		mSize = size;

		mComposerItem = new ListItemTitle(composer);
		AddChild(mComposerItem);
		
		mPlayItem = new ListItemImage("Images/play_blue.fw.png")
		{
			private static final long serialVersionUID = 8758715768537313883L;

			@Override
			public void Play()
			{
				AudioPlayer.getInstance().SetLocation(mURL, mSize).Play(mMarker1);
				InfoPanel.getInstance().SetInfo(mTitle, mComposer);
				setMarkers();
			}
		};
		mPlayItem.setBorder(new MatteBorder(0, 0, 0, 1, (Color) new Color(153, 153, 153)));
		AddChild(mPlayItem);
		mPlayItem.setImageVisible(false);
	}
	
	public void setMarkers()
	{
		InfoPanel.getInstance().SetMarkers(mMarker1);
	}
	
	@Override
	public void Select(Boolean origin)
	{
		super.Select(origin);
		AudioPlayer.getInstance().SetLocation(mURL, mSize).Play(10);
		//AudioPlayer.getInstance().SetLocation(mURL, mSize);
		InfoPanel.getInstance().SetInfo(mTitle, mComposer);
		setMarkers();
	}
	
	@Override
	public void MouseOver(Boolean origin)
	{
		super.MouseOver(origin);
		mPlayItem.setImageVisible(true);
	}
	
	@Override
	public void ClearOver(Boolean origin)
	{
		super.ClearOver(origin);
		mPlayItem.setImageVisible(false);
	}
	
	@Override
	public void AddToController(JPanel controllerPanel, int gridY)
	{
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.NORTH;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 0;
		c.gridx = 0;
		c.gridy = gridY;
		controllerPanel.add(this, c);
		
		c.gridx = 1;
		controllerPanel.add(mPlayItem, c);
		
		c.gridx = 2;
		controllerPanel.add(mComposerItem, c);
	}
	
	@Override
	public void RemoveFromController(JPanel controllerPanel)
	{
		controllerPanel.remove(this);
		controllerPanel.remove(mPlayItem);
		controllerPanel.remove(mComposerItem);
	}

}
