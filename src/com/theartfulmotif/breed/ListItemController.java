package com.theartfulmotif.breed;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.border.MatteBorder;

import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

public class ListItemController extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 540264259683481282L;

	List<ListItem> Items;
	int TabWidth;
	JPanel DisplayPanel;
	JPanel ItemPanel;
	
	/**
	 * Create the panel.
	 */
	public ListItemController(int width) {
		setBackground(Color.WHITE);
		this.setBorder(null);
		Items = new ArrayList<ListItem>();
		this.setPreferredSize(new Dimension(width,100));
		setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		add(panel, BorderLayout.CENTER);
		panel.setLayout(new BorderLayout(0, 0));

		DisplayPanel = new JPanel();
		DisplayPanel.setBorder(new MatteBorder(0, 0, 0, 1, (Color) new Color(153, 153, 153)));
		DisplayPanel.setBackground(Color.WHITE);
		DisplayPanel.setPreferredSize(new Dimension(width,100));
		DisplayPanel.setLayout(new BorderLayout(0,0));
		
		ItemPanel = new JPanel();
		ItemPanel.setOpaque(false);
		ItemPanel.setLayout(new GridBagLayout());
		DisplayPanel.add(ItemPanel, BorderLayout.NORTH);
		
		JScrollPane scrollPane = new JScrollPane(DisplayPanel);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBorder(BorderFactory.createEmptyBorder());
		panel.add(scrollPane, BorderLayout.CENTER);
		TabWidth = width;
	}
	
	public ListItemController()
	{
		setBackground(Color.WHITE);
		this.setBorder(null);
		Items = new ArrayList<ListItem>();
		this.setPreferredSize(new Dimension(100,100));
		setLayout(new BorderLayout(0, 0));
	}

	Boolean AltItem = false;
	
	public void AddItem(String title)
	{
		AddItem(title,false);
	}
	
	public void AddItem(String title, Boolean selfAdd)
	{
		ListItemTitle newItem = new ListItemTitle(title) {
			private static final long serialVersionUID = -1325717452779604403L;
			
			@Override
			public void Select(String title)
			{
				ListItemController.this.ClearSelect(this);
			}
		};
		newItem.Alt = AltItem;
		AltItem = !AltItem;
		InternalAddItem(newItem, selfAdd);
	}
	
	public void AddItem(ListItem item, Boolean selfAdd)
	{
		InternalAddItem(item, selfAdd);
	}
	
	public void AddItem(String title, String composer, String url, long marker1, int size)
	{
		TrackListItem newItem = new TrackListItem(title, composer, url, marker1, size) {
			private static final long serialVersionUID = 6578907472911464481L;
			
			@Override
			public void Select(String title)
			{
				ListItemController.this.ClearSelect(this);
			}
		};
		newItem.Alt = AltItem;
		AltItem = !AltItem;
		InternalAddItem(newItem, true);
	}
	
	public void RemoveAllItems()
	{
		for (ListItem item : Items)
		{
			InternalRemoveItem(item);
		}
		Items.clear();
	}
	
	private void InternalRemoveItem(ListItem item)
	{
		item.RemoveFromController(ItemPanel);
	}
	
	private void InternalAddItem(ListItem item, Boolean selfAdd)
	{
		if (selfAdd)
		{
			item.AddToController(ItemPanel, Items.size());
		}
		else
		{
			GridBagConstraints c = new GridBagConstraints();
			c.anchor = GridBagConstraints.NORTH;
			c.fill = GridBagConstraints.HORIZONTAL;
			c.weightx = 1;
			c.gridx = 0;
			c.gridy = Items.size();
			ItemPanel.add(item, c);
		}
		Items.add(item);
		DisplayPanel.setPreferredSize(new Dimension(TabWidth, 20 * Items.size()));
	}
	
	private void ClearSelect(ListItem excludeTab)
	{
		for (ListItem tab : Items)
		{
			if (tab != excludeTab)
				tab.Unselect(true);
		}
	}
}
