package com.theartfulmotif.breed;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class ButtonPanel extends JPanel implements MouseListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8716058749941826276L;
	
	private Boolean mOver = false;
	private JLabel mLabel;
	private Color mForeground;
	private Color mOverForeground;
	
	public ButtonPanel(String text)
	{
		mLabel = new JLabel(text);
		this.add(mLabel);
		
		this.setOpaque(false);
		
		this.addMouseListener(this);
	}
	
	public void setLabelForeground(Color color)
	{
		mForeground = color;
		mLabel.setForeground(color);
	}
	
	public void setLabelOverForeground(Color color)
	{
		mOverForeground = color;
	}
	
	public void OnClick()
	{
		
	}
	
	@Override
    protected void paintComponent(Graphics grphcs) {
		if (mOver)
		{
			Graphics2D g2d = (Graphics2D) grphcs;
	        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
	                RenderingHints.VALUE_ANTIALIAS_ON);
	        
	        GradientPaint gp = new GradientPaint(0, 0,
	                new Color(240,240,240), 0, getHeight(),
	                new Color(220,220,220));
	
	        g2d.setPaint(gp);
	        g2d.fillRect(0, 0, getWidth(), getHeight());
	
	        super.paintComponent(grphcs);
		}
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		OnClick();
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		mOver = true;
		mLabel.setForeground(mOverForeground);
		repaint();
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		mOver = false;
		mLabel.setForeground(mForeground);
		repaint();
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
