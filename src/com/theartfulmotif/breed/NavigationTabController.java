package com.theartfulmotif.breed;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

public class NavigationTabController extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 540264259683481282L;

	List<NavigationTab> NavTabs;
	int TabWidth;
	WorkspaceController Workspace;
	
	/**
	 * Create the panel.
	 */
	public NavigationTabController(int width, WorkspaceController workspace) {
		Workspace = workspace;
		NavTabs = new ArrayList<NavigationTab>();
		this.setPreferredSize(new Dimension(width,100));
		TabWidth = width;
		this.setOpaque(false);
	}

	public void AddTab(String title)
	{
		NavigationTab newTab = new NavigationTab(title, TabWidth) {
			private static final long serialVersionUID = -1325717452779604403L;
			@Override
			public void Select(String title)
			{
				NavigationTabController.this.ClearSelect(this);
				
				if (title == "Clients")
					Workspace.AddController(1);
				if (title == "All Tracks")
					Workspace.AddController(10);
			}
		};
		this.add(newTab);
		NavTabs.add(newTab);
	}
	
	public void ClearOver(NavigationTab excludeTab)
	{
		for (NavigationTab tab : NavTabs)
		{
			if (tab != excludeTab)
				tab.ClearOver();
		}
	}
	
	public void ClearSelect(NavigationTab excludeTab)
	{
		for (NavigationTab tab : NavTabs)
		{
			if (tab != excludeTab)
				tab.Unselect();
		}
	}
}
