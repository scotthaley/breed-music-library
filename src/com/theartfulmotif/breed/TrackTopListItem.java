package com.theartfulmotif.breed;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;

import javax.swing.JPanel;
import javax.swing.border.MatteBorder;

public class TrackTopListItem extends ListItem {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4478751669060298452L;
	
	private ListItemTitle mTitle;
	private ListItemTitle mComposer;
	private ListItem mPlayFiller;
	
	public TrackTopListItem()
	{
		super();
		mTitle = new ListItemTitle("Title");
		mTitle.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(153, 153, 153)));
		mTitle.setPreferredSize(new Dimension(200,20));
		mComposer = new ListItemTitle("Composer");
		mComposer.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(153, 153, 153)));
		mPlayFiller = new ListItem();
		mPlayFiller.setBorder(new MatteBorder(0, 0, 1, 1, (Color) new Color(153, 153, 153)));
		mPlayFiller.setPreferredSize(new Dimension(20,20));
		
		mTitle.AddChild(mPlayFiller);
	}

	
	@Override
	public void AddToController(JPanel controllerPanel, int gridY)
	{
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.NORTH;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 0;
		c.gridx = 0;
		c.gridy = gridY;
		controllerPanel.add(mTitle, c);
		
		c.gridx = 1;
		controllerPanel.add(mPlayFiller,c);
		
		c.weightx = 1;
		c.gridx = 2;
		controllerPanel.add(mComposer, c);
	}
	
	@Override
	public void RemoveFromController(JPanel controllerPanel)
	{
		controllerPanel.remove(mTitle);
		controllerPanel.remove(mPlayFiller);
		controllerPanel.remove(mComposer);
	}
}
