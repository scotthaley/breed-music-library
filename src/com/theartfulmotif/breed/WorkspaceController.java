package com.theartfulmotif.breed;

import java.awt.GridBagConstraints;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import com.theartfulmotif.breed.ListControllers.*;

public class WorkspaceController {
	
	JPanel WorkArea;
	List<ListItemController> ListControllers;
	
	ClientController Controller_Client;
	public TrackController Controller_Track;
	VideoPanel Panel_Video;
	ListItemControllerGrid Controller_Track_Video_Grid;
	
	public WorkspaceController(JPanel workArea)
	{
		WorkArea = workArea;
		ListControllers = new ArrayList<ListItemController>();
		
		Controller_Client = new ClientController();
		Controller_Track = new TrackController();
		Panel_Video = new VideoPanel();
		Controller_Track_Video_Grid = new ListItemControllerGrid();
		
		GridBagConstraints c = new GridBagConstraints();
		c.gridy = 0;
		c.gridx = 0;
		c.weighty = 1;
		c.weightx = 1;
		c.fill = GridBagConstraints.BOTH;
		Controller_Track_Video_Grid.Panel().add(Controller_Track, c);
		
		c.gridy = 1;
		c.weighty = 0;
		Controller_Track_Video_Grid.Panel().add(Panel_Video, c);
		
		AddControllerInternal(Controller_Track_Video_Grid, true);
	}
	
	public void AddController(int id)
	{
		if (id == 1)
			AddController(Controller_Client);
	}
	
	public void RemoveController(int id)
	{
		if (id == 1)
			RemoveController(Controller_Client);
	}

	private void AddController(ListItemController controller)
	{
		AddController(controller, false);
	}
	
	private void AddController(ListItemController controller, Boolean fillHorizontal)
	{
		RemoveControllerInternal(Controller_Track_Video_Grid);
		AddControllerInternal(controller, fillHorizontal);
		ListControllers.add(controller);
		AddControllerInternal(Controller_Track_Video_Grid, true);
	}
	
	private void AddControllerInternal(ListItemController controller, Boolean fillHorizontal)
	{
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = ListControllers.size() + 1;
		c.gridy = 0;
		c.anchor = GridBagConstraints.WEST;
		if (fillHorizontal)
		{
			c.anchor = GridBagConstraints.EAST;
			c.fill = GridBagConstraints.BOTH;
			c.weightx = 1.0;
		}
		else
			c.fill = GridBagConstraints.VERTICAL;	
		WorkArea.add(controller,c);
		WorkArea.revalidate();
	}
	
	private void RemoveController(ListItemController controller)
	{
		ListControllers.remove(controller);
		RemoveControllerInternal(controller);
	}
	
	private void RemoveControllerInternal(ListItemController controller)
	{
		WorkArea.remove(controller);
	}
}
