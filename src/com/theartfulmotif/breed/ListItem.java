package com.theartfulmotif.breed;

import java.awt.Dimension;

import javax.swing.JPanel;
import java.awt.FlowLayout;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

public class ListItem extends JPanel implements MouseListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -590427323572611292L;
	
	public boolean Alt = false;
	public boolean Over = false;
	public boolean Selected = false;
	public ListItem Parent = null;
	private List<ListItem> Children;

	/**
	 * Create the panel.
	 */
	public ListItem()
	{
		FlowLayout flowLayout = (FlowLayout) getLayout();
		flowLayout.setVgap(2);
		flowLayout.setHgap(15);
		flowLayout.setAlignment(FlowLayout.LEFT);
		this.setPreferredSize(new Dimension(20, 20));
		
		this.setOpaque(false);
		
		this.addMouseListener(this);
		
		Children = new ArrayList<ListItem>();
	}
	
	public ListItem(int hgap) {
		FlowLayout flowLayout = (FlowLayout) getLayout();
		flowLayout.setVgap(2);
		flowLayout.setHgap(hgap);
		flowLayout.setAlignment(FlowLayout.LEFT);
		this.setPreferredSize(new Dimension(20, 20));
		
		this.setOpaque(false);
		
		this.addMouseListener(this);
		
		Children = new ArrayList<ListItem>();
	}
	
	public void AddChild(ListItem item)
	{
		Children.add(item);
		item.Parent = this;
	}
	
	public void AddToController(JPanel controllerPanel, int gridY)
	{
		
	}
	
	public void RemoveFromController(JPanel controllerPanel)
	{
		controllerPanel.remove(this);
	}
	
	public void MouseOver(Boolean origin)
	{
		if (Parent != null && origin)
		{
			Parent.MouseOver(true);
		} else {
			Over = true;
			this.repaint();
			if (origin) {
				for (ListItem item : Children)
					item.MouseOver(false);
			}
		}
	}
	
	public void Select(String title)
	{
		
	}
	
	public void Select(Boolean origin)
	{
		if (Parent != null && origin)
		{
			Parent.Select(true);
		} else {
			Selected = true;
			this.repaint();
			if (origin) {
				for (ListItem item : Children)
					item.Select(false);
			}
		}
	}
	
	public void Unselect(Boolean origin)
	{
		if (Parent != null && origin)
		{
			Parent.Unselect(true);
		} else {
			Selected = false;
			this.repaint();
			if (origin) {
				for (ListItem item : Children)
					item.Unselect(false);
			}
		}
	}
	
	public void ClearOver(Boolean origin)
	{
		if (Parent != null && origin)
		{
			Parent.ClearOver(true);
		} else {
			Over = false;
			this.repaint();
			if (origin) {
				for (ListItem item : Children)
					item.ClearOver(false);
			}
		}
	}

	@Override
    protected void paintComponent(Graphics grphcs) {
		if (Selected)
		{
			Graphics2D g2d = (Graphics2D) grphcs;
	        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
	                RenderingHints.VALUE_ANTIALIAS_ON);
	        
	        Color color1 = new Color(180,180,180);
	        Color color2 = new Color(150,150,150);
	
	        GradientPaint gp = new GradientPaint(0, 0,
	                color1, 0, getHeight(),
	                color2);
	
	        g2d.setPaint(gp);
	        g2d.fillRect(0, 0, getWidth(), getHeight());
	
	        super.paintComponent(grphcs);
		}
		else if (Over)
		{
			Graphics2D g2d = (Graphics2D) grphcs;
	        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
	                RenderingHints.VALUE_ANTIALIAS_ON);
	        
	        GradientPaint gp = new GradientPaint(0, 0,
	                new Color(240,240,240), 0, getHeight(),
	                new Color(220,220,220));
	
	        g2d.setPaint(gp);
	        g2d.fillRect(0, 0, getWidth(), getHeight());
	
	        super.paintComponent(grphcs);
		} else if (Alt || Parent != null && Parent.Alt)
		{
			Graphics2D g2d = (Graphics2D) grphcs;
	        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
	                RenderingHints.VALUE_ANTIALIAS_ON);
	        
	        GradientPaint gp = new GradientPaint(0, 0,
	                new Color(240,240,255), 0, getHeight(),
	                new Color(240,240,255));
	
	        g2d.setPaint(gp);
	        g2d.fillRect(0, 0, getWidth(), getHeight());
	
	        super.paintComponent(grphcs);
		}
	}

	@Override
	public void mouseEntered(MouseEvent e)
	{
		MouseOver(true);
	}
	
	@Override
	public void mouseClicked(MouseEvent arg0) {
		Select(true);
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		ClearOver(true);
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
