package com.theartfulmotif.breed;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javaaudio.Player;

import javax.swing.Timer;


public class AudioPlayer {
	
	private static AudioPlayer instance = null;
	
	
	Player mPlayer;
	Boolean playing = false;
	Timer mTimer;
	String mLocation;

	public static AudioPlayer getInstance()
	{
		if (instance == null)
			instance = new AudioPlayer();
		return instance;
	}
	
	private AudioPlayer()
	{
		mPlayer = new Player();
		mPlayer.setCurrentVolume(1);
		
		ActionListener taskPerformer = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
            	Tick();
            }
		};
		
		mTimer = new Timer(10, taskPerformer);
	}
	
	private void Tick()
	{
		InfoPanel.getInstance().SetTotalTime(mPlayer.getTotalPlayTimeMcsec());
		if (InfoPanel.getInstance().TotalTime != 0)
			InfoPanel.getInstance().SetCurrentTime(mPlayer.getCurrentPosition());
		
		//System.out.println(mPlayer.getTotalPlayTimeMcsec());

	}
	
	public AudioPlayer SetLocation(String url)
	{
		return SetLocation(url, 0);
	}
	
	public AudioPlayer SetLocation(String url, int size)
	{
		if (url == mLocation)
		{
			Stop();
			Play(0);
			return this;
		}
		mLocation = url;
		mPlayer.setSourceLocation(url, size);
		return this;
	}
	
	public void Play(long pos)
	{
		Play();
		mPlayer.seek(pos);
	}
	
	public void Play()
	{
		InfoPanel.getInstance().TotalTime = 0;
		mPlayer.play();
		mTimer.start();
		playing = true;
	}
	
	public void Seek(long pos)
	{
		mPlayer.seek(pos);
		Play();
	}
	
	public void Stop()
	{
		playing = false;
		mPlayer.stop();
		mTimer.stop();
	}
}
