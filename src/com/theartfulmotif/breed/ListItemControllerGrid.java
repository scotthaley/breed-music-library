package com.theartfulmotif.breed;

import java.awt.BorderLayout;
import java.awt.GridBagLayout;

import javax.swing.JPanel;


public class ListItemControllerGrid extends ListItemController {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3739869947802982243L;
	
	JPanel gridPanel;
	
	public ListItemControllerGrid() {
		super();
		
		gridPanel = new JPanel();
		gridPanel.setLayout(new GridBagLayout());
		
		this.add(gridPanel, BorderLayout.CENTER);
	}
	
	public JPanel Panel()
	{
		return gridPanel;
	}

}
