package com.theartfulmotif.breed;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.border.MatteBorder;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Component;
import javax.swing.JButton;


public class MainWindow implements ComponentListener, ActionListener{

	private JFrame frmBreedMusicLibrary;
	//private JPanel InfoPanel;
	//private int ProgressBarWidth;
	//private JProgressBar Info_Progress;
	private WorkspaceController Workspace_Controller;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frmBreedMusicLibrary.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmBreedMusicLibrary = new JFrame();
		frmBreedMusicLibrary.setTitle("Breed Music Library");
		frmBreedMusicLibrary.setBounds(100, 100, 1280, 720);
		frmBreedMusicLibrary.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmBreedMusicLibrary.addComponentListener(this);
		
		JPanel PlaybackBar = new JPanel()
		{
			private static final long serialVersionUID = 7325709048930654118L;

			@Override
	            protected void paintComponent(Graphics grphcs) {
	                Graphics2D g2d = (Graphics2D) grphcs;
	                g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
	                        RenderingHints.VALUE_ANTIALIAS_ON);

	                GradientPaint gp = new GradientPaint(0, 0,
	                        getBackground().brighter(), 0, getHeight(),
	                        getBackground().darker());

	                g2d.setPaint(gp);
	                g2d.fillRect(0, 0, getWidth(), getHeight());

	                super.paintComponent(grphcs);
	            }
		};
		PlaybackBar.setOpaque(false);
		frmBreedMusicLibrary.getContentPane().add(PlaybackBar, BorderLayout.NORTH);
		
		PlaybackBar.add(InfoPanel.getInstance());
		
		JButton btnNewButton = new JButton("Upload Test");
		PlaybackBar.add(btnNewButton);
		btnNewButton.setActionCommand("upload");
		btnNewButton.addActionListener(this);

		JPanel WorkingArea = new JPanel();
		frmBreedMusicLibrary.getContentPane().add(WorkingArea, BorderLayout.CENTER);
		WorkingArea.setLayout(new GridBagLayout());
		
		JPanel NavigationPanel = new JPanel();
		NavigationPanel.setAlignmentY(Component.TOP_ALIGNMENT);
		NavigationPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		NavigationPanel.setBorder(new MatteBorder(0, 0, 0, 1, (Color) new Color(153, 153, 153)));
		NavigationPanel.setBackground(new Color(204, 222, 227));
		NavigationPanel.setPreferredSize(new Dimension(150, 200));
		GridBagConstraints gbc_NavigationPanel = new GridBagConstraints();
		gbc_NavigationPanel.anchor = GridBagConstraints.WEST;
		gbc_NavigationPanel.fill = GridBagConstraints.VERTICAL;
		gbc_NavigationPanel.gridx = 0;
		gbc_NavigationPanel.gridy = 0;
		gbc_NavigationPanel.weighty = 1;
		gbc_NavigationPanel.weightx = 0;
		WorkingArea.add(NavigationPanel, gbc_NavigationPanel);

		Workspace_Controller = new WorkspaceController(WorkingArea);
		
		NavigationTabController NavController = new NavigationTabController(150, Workspace_Controller);
		NavController.AddTab("All Tracks");
		NavController.AddTab("Clients");
		NavController.AddTab("Genres");
		NavController.AddTab("Composers");
		NavController.AddTab("Licensed");
		NavigationPanel.setLayout(new BorderLayout(0, 0));
		NavigationPanel.add(NavController);
		
	}
	
	public void actionPerformed(ActionEvent e)
	{
		if (e.getActionCommand() == "upload")
		{
			Dialog_TrackUpload uploadDialog = new Dialog_TrackUpload();
			uploadDialog.setTrackController(Workspace_Controller.Controller_Track);
			uploadDialog.setVisible(true);
		}
	}
	

	@Override
	public void componentHidden(ComponentEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void componentMoved(ComponentEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void componentResized(ComponentEvent arg0) {

	}

	@Override
	public void componentShown(ComponentEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
